# Handheld GPS Tracker SW
The goal is to create an autonomous handheld GPS tracker, that will provide us with information about our exact location, in order for us to find the rocket after its landing.
From the GPS Tracker on the rocket, using telemetry, we are going to receive the exact coordinates of the rocket, serially, in order to find it using our own location
# Description

An interface that continuously receives real-time data from the breakout board and displays it on a screen compatible with the size of the raspberry pi HAT. During data parsing, it collects data in NMEA format.The information needed from the NMEA message is saved in an output .txt file. The tracker’s exact coordinates respectively to the rocket are then displayed on a TFT LCD screen along with an orientation arrow that points directly to the rocket. Its ability to work offline makes it the ultimate autonomous device which will work efficiently under the competitions’ circumstances. 

