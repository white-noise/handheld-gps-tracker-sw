# -*- coding: utf-8 -*-

import pynmea2 
from PIL import  Image, ImageFont, ImageDraw, ImageFilter
import ST7735 as ST7735
import sys
import serial
import time
import string
import math
import numpy as np

'''
if len(sys.argv) != 2:
    exit(f"Usage: {sys.argv[0]} FILENAME")
filename = sys.argv[1]
'''

# We will have saved the 4 images into the r-pi's desktop so we can immediately 'call' them

#config image files 
if len(sys.argv) < 3: # 1st argument is the filename, 2nd is lat, 3rd is lon. Filename counts as argument
    print("Usage: Pass exact rocket lat and rocket lon as arguments")
    sys.exit(1)

rock_lat = sys.argv[1]
rock_lon = sys.argv[2]
# Create ST7735 LCD display class.
disp = ST7735.ST7735(
    port=0,
    cs=ST7735.BG_SPI_CS_FRONT,  # BG_SPI_CSB_BACK or BG_SPI_CS_FRONT or 0
    dc=9,                       # or 24
    backlight=None,             # 18 for back BG slot, 19 for front BG slot.
    rotation=0,
    spi_speed_hz=4000000,
    width=128,
    height=160,
    invert=False
    #,rst=25
)
WIDTH = disp.width
HEIGHT = disp.height

# we will insert the rocket at a radial distance of 60 pixels 
# (maximum distance/pixel for any location of the rocket on the screen)
# we will constantly update the distance/pixel ratio so that when we reach, for example
# 1/30th of the original distance, but are still far away (like 100 m) we 
# can still see where we need to go towards

disp.begin()                    
font = ImageFont.load_default()  

bg = Image.open(r"~/Desktop/White_Background.png").resize((128,160))# background: explained below
trckr_img = Image.open(r"~/Desktop/tracker.png").resize((9,15))     # the aspect ratio is about 3:5
crns_img = Image.open(r"~/Desktop/cronos.png").resize((7,14))       # the aspect ratio is about 1:2
arrw_img = Image.open(r"~/Desktop/arrow.png").resize((20,60))       # this is an arrow, so we don't care
                                                                    # that much about aspect ratio
arrw_img = arrw_img.convert("RGBA") # the A in RGBA is transparancy

# Why did we import a background? PIL (to our knowledge) doesn't let us move images, only display
# onto different images, onto different parts of different images. So, if we import a bg, we can
# print the existing images wherever we want onto the background, thus, the whole display

'''
im.paste(crns_img, (a, b, c, d))
whereas 
a = left
b = upper
c = right
d = lower

Pastes another image into this image. The box argument is either a 2-tuple giving the upper left corner,
a 4-tuple defining the left, upper, right, and lower pixel coordinate, or None (same as (0, 0)). 
If a 4-tuple is given, the size of the pasted image must match the size of the region

so basically, a-c must be equal to image width
b - d must be equal to image height
'''

lat_prev = 0
lon_prev = 0 # initialising so we can do the initial calculations (junk data) without segmentation faults
while True   
    try:
        msg = pynmea2.NMEAStreamReader()
        newdata=ser.readline()
        if (newdata[0:6] == b"$GPGLL"): #best for lat/lon
            newmsg=pynmea2.parse(newdata.decode("utf-8"))
                lat=newmsg.latitude
                lon=newmsg.longitude
        if (! lat_prev):    # we need to establish the previous position, 
                            # and it needs to not be 0, else we'll get random outputs

            lat_dif = rock_lat - lat # latitude difference
            lon_dif = rock_lon - lon # longitude difference
            lat_dif_2 = pow(lat_dif, 2)
            lon_dif_2 = pow(lon_dif, 2) # for the pythagorean theorem
            pxl_ratio = (math.sqrt(lat_dif_2 + lon_dif_2))/60 # current distance to pixel ratio
            rock_pxl_height = int(lat_dif / pxl_ratio) # should be less than 61 and more than -61 in all cases
            rock_pxl_width = int(lon_dif / pxl_ratio) # should be less than 61 and more than -61 in all cases
            bg.paste(crns_img, (rock_pxl_width, rock_pxl_height))
            #bg.save

            dist_lat = lat - lat_prev
            dist_lon = lon - lon_prev
            answer1 = math.atan(dist_lon/dist_lat)
            arrw_img = Image.open(r"~/Desktop/arrow.png").resize((20,60)).rotate(int(360 - math.degrees(answer1)), expand=1)
            arrw_img = arrw_img.convert("RGBA")
            
            # Buffer to display hardware
            disp.display(bg, trckr_img, crns_img, arrw_img)
            lat_prev = lat
            lon_prev = lon


    except pynmea2.ParseError as e:
        # print('Parse error: {}'.format(e))
        # continue
        pass




# How to draw an image 
#img = Image.new('RGB', (WIDTH, HEIGHT))
#draw = ImageDraw.Draw(img)






